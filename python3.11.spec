%global src_version 3.11.6
%global pybasever %%(echo %{src_version} | cut -d. -f1-2)
%global pyshortver %%(echo %{pybasever} | tr -d '.')
%global pip_version 23.2.1
%global setuptools_version 65.5.0

%if "%{?__default_python3_pkgversion}" == "%{pybasever}"
%bcond_without main_python
%else
%bcond_with main_python
%endif

%bcond_with bootstrap
%if %{with bootstrap}
%bcond_with rpmwheels
%else
%bcond_without rpmwheels
%endif

%bcond_without optimizations
%bcond_without tests
%bcond_without gdb_hooks
%bcond_without gdbm
%bcond_without computed_gotos
%bcond_without debug_build

%ifarch %{valgrind_arches}
%bcond_without valgrind
%else
%bcond_with valgrind
%endif

%if %{with main_python}
%global pkgname python3
%else
%global pkgname python%{pybasever}
%endif

%global pylibdir %{_libdir}/python%{pybasever}
%global dynload_dir %{pylibdir}/lib-dynload
%global ABIFLAGS_debug     d
%global LDVERSION_optimized %{pybasever}
%global LDVERSION_debug     %{pybasever}%{ABIFLAGS_debug}

%global platform_triplet %{expand:%(echo %{_arch}-linux%{_gnu} | sed -E \\
    -e 's/^arm(eb)?-linux-gnueabi$/arm\\1-linux-gnueabihf/' \\
    -e 's/^mips64(el)?-linux-gnu$/mips64\\1-linux-gnuabi64/' \\
    -e 's/^ppc(64)?(le)?-linux-gnu$/powerpc\\1\\2-linux-gnu/')}

%global SOABI_optimized cpython-%{pyshortver}-%{platform_triplet}
%global SOABI_debug     cpython-%{pyshortver}%{ABIFLAGS_debug}-%{platform_triplet}
%global bytecode_suffixes .cpython-%{pyshortver}*.pyc
%global py_SOVERSION 1.0
%global py_INSTSONAME_optimized libpython%{LDVERSION_optimized}.so.%{py_SOVERSION}
%global py_INSTSONAME_debug     libpython%{LDVERSION_debug}.so.%{py_SOVERSION}

%undefine py_auto_byte_compile
%undefine __pythonname_provides
%undefine _debuginfo_subpackages
%undefine _include_frame_pointers



Summary: Version %{pybasever} of the Python interpreter
Name: python%{pybasever}
Version: %{src_version}
Release: 16%{?dist}
License: Python-2.0.1
URL: https://www.python.org/

Source0: %{url}ftp/python/%{version}/Python-%{version}.tar.xz
# Idle is the official IDE of Python
Source10: idle3.desktop
Patch0001: backport-gh-115133-fix-tests-for-xmlpullparser-with-expat-2-6-0.patch
Patch0002: CVE-2024-0450.patch
Patch0003: CVE-2024-0397-3.11-gh-114572-Fix-locking-in-cert_store_stats-and-g.patch
Patch0004: CVE-2024-4032-3.11-gh-113171-gh-65056-Fix-private-non-global-IP-ad.patch
Patch0005: CVE-2024-6232-3.11-gh-121285-Remove-backtracking-when-parsing-tarf.patch
Patch0006: CVE-2023-6597.patch
Patch0007: CVE-2024-8088.patch
Patch0008: CVE-2024-7592.patch
Patch0009: CVE-2024-6923.patch
Patch0010: CVE-2024-9287-3.11-gh-124651-Quote-template-strings-in-venv-activa.patch
Patch0011: CVE-2025-0938.patch
Patch0012: CVE-2025-1795-3.11-gh-100884-email-_header_value_parser-don-t-enco.patch
Patch0013: 0001-gh-113594-Fix-UnicodeEncodeError-in-TokenList.fold-G.patch
Patch0014: 0001-gh-92081-Fix-for-email.generator.Generator-with-whit.patch
# fix bug introduced in CVE-2025-1795, Patch0013 - 0014 is pre-patch for Patch0015
Patch0015: 0001-gh-118643-Fix-AttributeError-in-the-email-module-GH-.patch

Patch3000: 00001-rpath.patch
Patch3001: 00251-change-user-install-location.patch
Patch3002: 00371-revert-bpo-1596321-fix-threading-_shutdown-for-the-main-thread-gh-28549-gh-28589.patch
Patch3003: python3.11-add-loongarch64-support.patch

BuildRequires: autoconf, make, pkgconfig, gdb, gcc-c++, findutils, glibc-all-langpacks, glibc-devel
BuildRequires: bzip2, bzip2-devel, tar, zlib-devel, expat-devel, xz-devel
BuildRequires: desktop-file-utils, libGL-devel, libX11-devel, tcl-devel, tix-devel, tk-devel
BuildRequires: git-core, gmp-devel, libappstream-glib, libb2-devel, libffi-devel, libnsl2-devel, libtirpc-devel
BuildRequires: libuuid-devel, mpdecimal-devel, ncurses-devel, openssl-devel, readline-devel, sqlite-devel, system-rpm-config
BuildRequires: tzdata, systemtap-sdt-devel, net-tools, bluez-libs-devel
%if %{with gdbm}
BuildRequires: gdbm-devel
%endif
%if %{with valgrind}
BuildRequires: valgrind-devel
%endif

%if %{with rpmwheels}
BuildRequires: %{python_wheel_pkg_prefix}-setuptools-wheel
BuildRequires: %{python_wheel_pkg_prefix}-pip-wheel
%endif

%if %{without bootstrap}
BuildRequires: python%{pybasever}
BuildRequires: python3-rpm-generators
%endif



%if %{with main_python}
%description
Python is a great object-oriented, interpreted, and interactive programming
language. It is often compared (favorably of course :-) ) to Lisp, Tcl, 
Perl, Ruby, C#, Visual Basic, Visual Fox Pro, Scheme or Java... and it's 
much more fun.

Python combines remarkable power with very clear syntax. It has modules, 
classes, exceptions, very high level dynamic data types, and dynamic typing. 
There are interfaces to many system calls and libraries, as well as to various 
windowing systems. New built-in modules are easily written in C or C++ (or 
other languages, depending on the chosen implementation). Python is also 
usable as an extension language for applications written in other languages 
that need easy-to-use scripting or automation interfaces. 

%package -n %{pkgname}
Summary: Python %{pybasever} interpreter
Provides: python%{pybasever} = %{version}-%{release}
Provides: python%{pyshortver} = %{version}-%{release}
Provides: python(abi) = %{pybasever}
Provides: python-is-python3 = %{version}-%{release}
Provides: python = %{version}-%{release}

# default install in user environment and not install in koji
Recommends: %{_bindir}/python
%else
%global __requires_exclude ^python\\(abi\\) = 3\\..+
%global __provides_exclude ^python\\(abi\\) = 3\\..+
%endif

Requires: %{pkgname}-libs = %{version}-%{release}
%global __requires_exclude ^(/usr/bin/python3|python\\(abi\\))


%description -n %{pkgname}
Python is a great object-oriented, interpreted, and interactive programming
language. It is often compared (favorably of course :-) ) to Lisp, Tcl, 
Perl, Ruby, C#, Visual Basic, Visual Fox Pro, Scheme or Java... and it's 
much more fun.

Python combines remarkable power with very clear syntax. It has modules, 
classes, exceptions, very high level dynamic data types, and dynamic typing. 
There are interfaces to many system calls and libraries, as well as to various 
windowing systems. New built-in modules are easily written in C or C++ (or 
other languages, depending on the chosen implementation). Python is also 
usable as an extension language for applications written in other languages 
that need easy-to-use scripting or automation interfaces. 

This package contains the Python3 interpreter.


%package -n %{pkgname}-libs
Summary:        Python runtime libraries
%if %{with rpmwheels}
Requires: %{python_wheel_pkg_prefix}-setuptools-wheel
Requires: %{python_wheel_pkg_prefix}-pip-wheel
%else
Requires: tzdata
Provides: bundled(python3dist(pip)) = %{pip_version}
Provides: bundled(python3dist(setuptools)) = %{setuptools_version}
%endif

Recommends: %{pkgname} = %{version}-%{release}
Recommends: (%{pkgname}-tkinter = %{version}-%{release} if tk)

%description -n %{pkgname}-libs
Python is a great object-oriented, interpreted, and interactive programming
language. It is often compared (favorably of course :-) ) to Lisp, Tcl, 
Perl, Ruby, C#, Visual Basic, Visual Fox Pro, Scheme or Java... and it's 
much more fun.

Python combines remarkable power with very clear syntax. It has modules, 
classes, exceptions, very high level dynamic data types, and dynamic typing. 
There are interfaces to many system calls and libraries, as well as to various 
windowing systems. New built-in modules are easily written in C or C++ (or 
other languages, depending on the chosen implementation). Python is also 
usable as an extension language for applications written in other languages 
that need easy-to-use scripting or automation interfaces. 

This package contains Python C/C++ libraries by apps and Python standard
library.


%package -n %{pkgname}-devel
Summary: Libraries and header files needed for Python development
Requires: %{pkgname} = %{version}-%{release}
Requires: %{pkgname}-libs = %{version}-%{release}
Requires: (python-rpm-macros if rpm-build)
Requires: (python3-rpm-macros if rpm-build)
Requires: (pyproject-rpm-macros if rpm-build)
Requires: (python3-rpm-generators if rpm-build)

%if %{with main_python}
Recommends: python3-pip
Provides: 2to3 = %{version}-%{release}
%endif

Supplements: tox
Provides: %{pkgname}-2to3 = %{version}-%{release}

%description -n %{pkgname}-devel
Python is a great object-oriented, interpreted, and interactive programming
language. It is often compared (favorably of course :-) ) to Lisp, Tcl, 
Perl, Ruby, C#, Visual Basic, Visual Fox Pro, Scheme or Java... and it's 
much more fun.

Python combines remarkable power with very clear syntax. It has modules, 
classes, exceptions, very high level dynamic data types, and dynamic typing. 
There are interfaces to many system calls and libraries, as well as to various 
windowing systems. New built-in modules are easily written in C or C++ (or 
other languages, depending on the chosen implementation). Python is also 
usable as an extension language for applications written in other languages 
that need easy-to-use scripting or automation interfaces. 

This package contains the development files.


%package -n %{pkgname}-idle
Summary: A basic graphical development environment for Python
Requires: %{pkgname} = %{version}-%{release}
Requires: %{pkgname}-tkinter = %{version}-%{release}
Provides: %{pkgname}-tools = %{version}-%{release}

%if %{with main_python}
Provides: idle3 = %{version}-%{release}
Provides: idle = %{version}-%{release}
%endif

%description -n %{pkgname}-idle
Python is a great object-oriented, interpreted, and interactive programming
language. It is often compared (favorably of course :-) ) to Lisp, Tcl, 
Perl, Ruby, C#, Visual Basic, Visual Fox Pro, Scheme or Java... and it's 
much more fun.

Python combines remarkable power with very clear syntax. It has modules, 
classes, exceptions, very high level dynamic data types, and dynamic typing. 
There are interfaces to many system calls and libraries, as well as to various 
windowing systems. New built-in modules are easily written in C or C++ (or 
other languages, depending on the chosen implementation). Python is also 
usable as an extension language for applications written in other languages 
that need easy-to-use scripting or automation interfaces. 

IDLE is the Python IDE built with the Tkinter GUI toolkit. 


%package -n %{pkgname}-tkinter
Summary: A GUI toolkit for Python
Requires: %{pkgname} = %{version}-%{release}
Provides: %{pkgname}-turtle = %{version}-%{release}

%description -n %{pkgname}-tkinter
Python is a great object-oriented, interpreted, and interactive programming
language. It is often compared (favorably of course :-) ) to Lisp, Tcl, 
Perl, Ruby, C#, Visual Basic, Visual Fox Pro, Scheme or Java... and it's 
much more fun.

Python combines remarkable power with very clear syntax. It has modules, 
classes, exceptions, very high level dynamic data types, and dynamic typing. 
There are interfaces to many system calls and libraries, as well as to various 
windowing systems. New built-in modules are easily written in C or C++ (or 
other languages, depending on the chosen implementation). Python is also 
usable as an extension language for applications written in other languages 
that need easy-to-use scripting or automation interfaces. 

Tkinter is the standard Python interface to the Tcl/Tk GUI toolkit.


%package -n %{pkgname}-test
Summary: The self-test suite for the main python3 package
Requires: %{pkgname} = %{version}-%{release}
Requires: %{pkgname}-libs = %{version}-%{release}

%description -n %{pkgname}-test
Python is a great object-oriented, interpreted, and interactive programming
language. It is often compared (favorably of course :-) ) to Lisp, Tcl, 
Perl, Ruby, C#, Visual Basic, Visual Fox Pro, Scheme or Java... and it's 
much more fun.

Python combines remarkable power with very clear syntax. It has modules, 
classes, exceptions, very high level dynamic data types, and dynamic typing. 
There are interfaces to many system calls and libraries, as well as to various 
windowing systems. New built-in modules are easily written in C or C++ (or 
other languages, depending on the chosen implementation). Python is also 
usable as an extension language for applications written in other languages 
that need easy-to-use scripting or automation interfaces. 

This package contains self-test suite for the Python interpreter.


%if %{with debug_build}
%package -n %{pkgname}-debug
Summary: Debug version of the Python runtime
Requires: %{pkgname} = %{version}-%{release}
Requires: %{pkgname}-libs = %{version}-%{release}
Requires: %{pkgname}-devel = %{version}-%{release}
Requires: %{pkgname}-test = %{version}-%{release}
Requires: %{pkgname}-tkinter = %{version}-%{release}
Requires: %{pkgname}-idle = %{version}-%{release}

%description -n %{pkgname}-debug
Python is a great object-oriented, interpreted, and interactive programming
language. It is often compared (favorably of course :-) ) to Lisp, Tcl, 
Perl, Ruby, C#, Visual Basic, Visual Fox Pro, Scheme or Java... and it's 
much more fun.

Python combines remarkable power with very clear syntax. It has modules, 
classes, exceptions, very high level dynamic data types, and dynamic typing. 
There are interfaces to many system calls and libraries, as well as to various 
windowing systems. New built-in modules are easily written in C or C++ (or 
other languages, depending on the chosen implementation). Python is also 
usable as an extension language for applications written in other languages 
that need easy-to-use scripting or automation interfaces. 

This package contains a Python runtime with numerous debugging features 
enabled, aimed at advanced Python users such as developers of Python
extension modules.
%endif

%if %{with main_python}
%package -n python-unversioned-command
Summary: The "python" command that runs Python 3
BuildArch: noarch

Requires: python3 == %{version}-%{release}
Provides: python = %{version}-%{release}
Provides: python-is-python3 = %{version}-%{release}

%description -n python-unversioned-command
This package contains /usr/bin/python - the "python" command that runs Python 3.
%endif

%prep
%autosetup -p1 -n Python-%{version}

%if %{with rpmwheels}
rm Lib/ensurepip/_bundled/pip-%{pip_version}-py3-none-any.whl
rm Lib/ensurepip/_bundled/setuptools-%{setuptools_version}-py3-none-any.whl
%endif

find -name '*.exe' -print -delete
rm -r Modules/expat
rm -r Modules/_decimal/libmpdec
rm configure pyconfig.h.in



%build
export HAS_GIT=not-found
autoconf
autoheader

%if %{with computed_gotos}
%global computed_gotos_flag yes
%else
%global computed_gotos_flag no
%endif

%if %{with optimizations}
%global optimizations_flag "--enable-optimizations"
%else
%global optimizations_flag "--disable-optimizations"
%endif

export CFLAGS="%{extension_cflags}"
export CFLAGS_NODIST="%{build_cflags} -D_GNU_SOURCE -fPIC -fwrapv"
export CXXFLAGS="%{extension_cxxflags}"
export CPPFLAGS="$(pkg-config --cflags-only-I libffi)"
export OPT="%{extension_cflags}"
export LINKCC="gcc"
export CFLAGS="$CFLAGS $(pkg-config --cflags openssl)"
export LDFLAGS="%{extension_ldflags} $(pkg-config --libs-only-L openssl)"
export LDFLAGS_NODIST="%{build_ldflags} -g $(pkg-config --libs-only-L openssl)"
export DFLAGS=" "

topdir=$(pwd)
mkdir -p normal
pushd normal
%global _configure ${topdir}/configure

%configure \
  --with-platlibdir=%{_lib} \
  --enable-ipv6 \
  --enable-shared \
  --with-computed-gotos=%{computed_gotos_flag} \
  --with-dbmliborder=gdbm:ndbm:bdb \
  --with-system-expat \
  --with-system-ffi \
  --with-system-libmpdec \
  --enable-loadable-sqlite-extensions \
  --with-dtrace \
  --with-lto \
  --with-ssl-default-suites=openssl \
  --without-static-libpython \
%if %{with rpmwheels}
  --with-wheel-pkg-dir=%{python_wheel_dir} \
%endif
%if %{with valgrind}
  --with-valgrind \
%endif
  --without-ensurepip %{optimizations_flag}

%if %{without bootstrap}
  %make_build CFLAGS_NODIST="$CFLAGS_NODIST" regen-all PYTHON_FOR_REGEN="python%{pybasever}"
%endif

%make_build CFLAGS_NODIST="$CFLAGS_NODIST"
popd

# debug build
%if %{with debug_build}
topdir=$(pwd)

mkdir -p debug
pushd debug
%global _configure ${topdir}/configure

%configure \
  --with-platlibdir=%{_lib} \
  --enable-ipv6 \
  --enable-shared \
  --with-computed-gotos=%{computed_gotos_flag} \
  --with-dbmliborder=gdbm:ndbm:bdb \
  --with-system-expat \
  --with-system-ffi \
  --with-system-libmpdec \
  --enable-loadable-sqlite-extensions \
  --with-dtrace \
  --with-lto \
  --with-ssl-default-suites=openssl \
  --without-static-libpython \
%if %{with rpmwheels}
  --with-wheel-pkg-dir=%{python_wheel_dir} \
%endif
%if %{with valgrind}
  --with-valgrind \
%endif
  --without-ensurepip \
  --with-pydebug

%if %{without bootstrap}	
  %make_build EXTRA_CFLAGS="-O0 -Wno-cpp" CFLAGS_NODIST="$CFLAGS_NODIST -O0 -Wno-cpp" regen-all PYTHON_FOR_REGEN="python%{pybasever}"
%endif
	
%make_build EXTRA_CFLAGS="-O0 -Wno-cpp" CFLAGS_NODIST="$CFLAGS_NODIST -O0 -Wno-cpp"
popd
%endif



%install
%if %{with gdb_hooks}
DirHoldingGdbPy=%{_usr}/lib/debug/%{_libdir}
mkdir -p %{buildroot}$DirHoldingGdbPy
%endif

%global _pyconfig32_h pyconfig-32.h
%global _pyconfig64_h pyconfig-64.h
%global _pyconfig_h pyconfig-%{__isa_bits}.h

#
# debug install
#

%if %{with debug_build}
pushd debug
%make_install EXTRA_CFLAGS="-O0"
popd

LDVersion=%{LDVERSION_debug}

%if %{with gdb_hooks}
  PathOfGdbPy=$DirHoldingGdbPy/%{py_INSTSONAME_debug}-%{version}-%{release}.%{_arch}.debug-gdb.py
  cp Tools/gdb/libpython.py %{buildroot}$PathOfGdbPy
%endif

  mv %{buildroot}%{_bindir}/python${LDVersion}-{,`uname -m`-}config
  echo -e '#!/bin/sh\nexec %{_bindir}/python'${LDVersion}'-`uname -m`-config "$@"' > \
    %{buildroot}%{_bindir}/python${LDVersion}-config
    chmod +x %{buildroot}%{_bindir}/python${LDVersion}-config

  mv %{buildroot}%{_includedir}/python${LDVersion}/pyconfig.h \
     %{buildroot}%{_includedir}/python${LDVersion}/%{_pyconfig_h}
  cat > %{buildroot}%{_includedir}/python${LDVersion}/pyconfig.h << EOF
#include <bits/wordsize.h>

#if __WORDSIZE == 32
#include "%{_pyconfig32_h}"
#elif __WORDSIZE == 64
#include "%{_pyconfig64_h}"
#else
#error "Unknown word size"
#endif
EOF
%endif

#
# normal install
#
pushd normal
%make_install
popd

%if %{with gdb_hooks}
  PathOfGdbPy=$DirHoldingGdbPy/%{py_INSTSONAME_optimized}-%{version}-%{release}.%{_arch}.debug-gdb.py
  cp Tools/gdb/libpython.py %{buildroot}$PathOfGdbPy
%endif

mv %{buildroot}%{_bindir}/python%{LDVERSION_optimized}-{,`uname -m`-}config
echo -e '#!/bin/sh\nexec %{_bindir}/python'%{pybasever}'-`uname -m`-config "$@"' > \
  %{buildroot}%{_bindir}/python%{LDVERSION_optimized}-config
  chmod +x %{buildroot}%{_bindir}/python%{LDVERSION_optimized}-config

mv %{buildroot}%{_includedir}/python%{LDVERSION_optimized}/pyconfig.h \
    %{buildroot}%{_includedir}/python%{LDVERSION_optimized}/%{_pyconfig_h}
cat > %{buildroot}%{_includedir}/python%{LDVERSION_optimized}/pyconfig.h << EOF
#include <bits/wordsize.h>

#if __WORDSIZE == 32
#include "%{_pyconfig32_h}"
#elif __WORDSIZE == 64
#include "%{_pyconfig64_h}"
#else
#error "Unknown word size"
#endif
EOF

install -d -m 0755 %{buildroot}%{pylibdir}/site-packages/__pycache__
%if "%{_lib}" == "lib64"
install -d -m 0755 %{buildroot}%{_prefix}/lib/python%{pybasever}/site-packages/__pycache__
%endif

%if %{with main_python}
install -D -m 0644 Lib/idlelib/Icons/idle_16.png %{buildroot}%{_datadir}/icons/hicolor/16x16/apps/idle3.png
install -D -m 0644 Lib/idlelib/Icons/idle_32.png %{buildroot}%{_datadir}/icons/hicolor/32x32/apps/idle3.png
install -D -m 0644 Lib/idlelib/Icons/idle_48.png %{buildroot}%{_datadir}/icons/hicolor/48x48/apps/idle3.png
install -D -m 0644 Lib/idlelib/Icons/idle_256.png %{buildroot}%{_datadir}/icons/hicolor/256x256/apps/idle3.png
desktop-file-install --dir=%{buildroot}%{_datadir}/applications %{SOURCE10}
%endif

sed -i -e "s/'pyconfig.h'/'%{_pyconfig_h}'/" \
  %{buildroot}%{pylibdir}/distutils/sysconfig.py \
  %{buildroot}%{pylibdir}/sysconfig.py

cp -p Tools/scripts/pathfix.py %{buildroot}%{_bindir}/pathfix%{pybasever}.py

for tool in pygettext msgfmt; do
  cp -p Tools/i18n/${tool}.py %{buildroot}%{_bindir}/${tool}%{pybasever}.py
  ln -s ${tool}%{pybasever}.py %{buildroot}%{_bindir}/${tool}3.py
done

LD_LIBRARY_PATH=./normal ./normal/python \
  Tools/scripts/pathfix.py \
  -i "%{_bindir}/python%{pybasever}" -pn \
  %{buildroot} \
  %{buildroot}%{_bindir}/*%{pybasever}.py \
  %{?with_gdb_hooks:%{buildroot}$DirHoldingGdbPy/*.py}

find %{buildroot} -name \*.py \
  \( \( \! -perm /u+x,g+x,o+x -exec sed -e '/^#!/Q 0' -e 'Q 1' {} \; \
  -print -exec sed -i '1d' {} \; \) -o \( \
  -perm /u+x,g+x,o+x ! -exec grep -m 1 -q '^#!' {} \; \
  -exec chmod a-x {} \; \) \)

find %{buildroot} -name \*.bat -exec rm {} \;
find %{buildroot}/ -name "*~" -exec rm -f {} \;
find . -name "*~" -exec rm -f {} \;

%if 0%{?clamp_mtime_to_source_date_epoch}
LD_LIBRARY_PATH="%{buildroot}%{dynload_dir}/:%{buildroot}%{_libdir}" \
PYTHONPATH="%{_rpmconfigdir}/%{_vendor}" \
%{buildroot}%{_bindir}/python%{pybasever} -s -B -m clamp_source_mtime %{buildroot}%{pylibdir}
%endif

LD_LIBRARY_PATH="%{buildroot}%{dynload_dir}/:%{buildroot}%{_libdir}" \
%{buildroot}%{_bindir}/python%{pybasever} -s -B -m compileall \
-f %{_smp_mflags} -o 0 -o 1 -o 2 -s %{buildroot} -p / %{buildroot} --hardlink-dupes --invalidation-mode=timestamp \
-x 'bad_coding|badsyntax|site-packages|lib2to3/tests/data'

%global __brp_python_hardlink %{nil}

rm -rf %{buildroot}%{_bindir}/__pycache__

find %{buildroot} -perm 555 -exec chmod 755 {} \;

%if %{with debug_build} && %{with main_python}	
ln -s  %{_bindir}/python%{LDVERSION_debug}  %{buildroot}%{_bindir}/python3-debug	
ln -s ./python3-debug %{buildroot}%{_bindir}/python-debug
%endif

%if %{without main_python}
rm %{buildroot}%{_bindir}/python3
rm %{buildroot}%{_bindir}/pydoc3
rm %{buildroot}%{_bindir}/pygettext3.py
rm %{buildroot}%{_bindir}/msgfmt3.py
rm %{buildroot}%{_bindir}/idle3
rm %{buildroot}%{_bindir}/python3-*
rm %{buildroot}%{_bindir}/2to3
rm %{buildroot}%{_libdir}/libpython3.so
rm %{buildroot}%{_mandir}/man1/python3.1
rm %{buildroot}%{_libdir}/pkgconfig/python3.pc
rm %{buildroot}%{_libdir}/pkgconfig/python3-embed.pc
%else
ln -s ./python3 %{buildroot}%{_bindir}/python
ln -s ./pydoc3 %{buildroot}%{_bindir}/pydoc
ln -s ./pygettext3.py %{buildroot}%{_bindir}/pygettext.py
ln -s ./msgfmt3.py %{buildroot}%{_bindir}/msgfmt.py
ln -s ./idle3 %{buildroot}%{_bindir}/idle
ln -s ./python3-config %{buildroot}%{_bindir}/python-config
ln -s ./python3.1 %{buildroot}%{_mandir}/man1/python.1
ln -s ./python3.pc %{buildroot}%{_libdir}/pkgconfig/python.pc
ln -s ./pathfix%{pybasever}.py %{buildroot}%{_bindir}/pathfix.py
%endif

for file in %{buildroot}%{pylibdir}/pydoc_data/topics.py $(grep --include='*.py' -lr %{buildroot}%{pylibdir}/encodings -e 'Python Character Mapping Codec .* from .* with gencodec.py'); do
    directory=$(dirname ${file})
    module=$(basename ${file%%.py})
    mv ${directory}/{__pycache__/${module}.cpython-%{pyshortver}.pyc,${module}.pyc}
    rm ${directory}/{__pycache__/${module}.cpython-%{pyshortver}.opt-?.pyc,${module}.py}
done



%check
# if zope related python packages were installed in system, test will be failed on test_embed case.
LD_LIBRARY_PATH=$(pwd)/normal $(pwd)/normal/python -m test.pythoninfo

LD_LIBRARY_PATH=$(pwd)/normal $(pwd)/normal/python -m test.regrtest \
  -wW --slowest -j0 --timeout=1800 \
  -i test_freeze_simple_script \
  -i test_check_probes \
  %if %{with bootstrap}
  -x test_distutils \
  %endif



%files -n %{pkgname}
%doc README.rst
%if %{with main_python}
%{_bindir}/pydoc*
%{_bindir}/python3
%else
%{_bindir}/pydoc%{pybasever}
%endif

%{_bindir}/python%{pybasever}
%{_bindir}/python%{LDVERSION_optimized}
%{_mandir}/*/*3*

%files -n %{pkgname}-libs
%license %{pylibdir}/LICENSE.txt
%doc README.rst
%dir %{pylibdir}
%dir %{dynload_dir}
%{pylibdir}/lib2to3/
%exclude %{pylibdir}/lib2to3/tests

%dir %{pylibdir}/unittest/
%dir %{pylibdir}/unittest/__pycache__/
%{pylibdir}/unittest/*.py
%{pylibdir}/unittest/__pycache__/*%{bytecode_suffixes}

%dir %{pylibdir}/asyncio/
%dir %{pylibdir}/asyncio/__pycache__/
%{pylibdir}/asyncio/*.py
%{pylibdir}/asyncio/__pycache__/*%{bytecode_suffixes}

%dir %{pylibdir}/venv/
%dir %{pylibdir}/venv/__pycache__/
%{pylibdir}/venv/*.py
%{pylibdir}/venv/__pycache__/*%{bytecode_suffixes}
%{pylibdir}/venv/scripts
%{pylibdir}/wsgiref
%{pylibdir}/xmlrpc

%dir %{pylibdir}/ensurepip/
%dir %{pylibdir}/ensurepip/__pycache__/
%{pylibdir}/ensurepip/*.py
%{pylibdir}/ensurepip/__pycache__/*%{bytecode_suffixes}

%if %{with rpmwheels}
%exclude %{pylibdir}/ensurepip/_bundled
%else
%dir %{pylibdir}/ensurepip/_bundled
%{pylibdir}/ensurepip/_bundled/pip-%{pip_version}-py3-none-any.whl
%{pylibdir}/ensurepip/_bundled/setuptools-%{setuptools_version}-py3-none-any.whl
%endif

%dir %{pylibdir}/concurrent/
%dir %{pylibdir}/concurrent/__pycache__/
%{pylibdir}/concurrent/*.py
%{pylibdir}/concurrent/__pycache__/*%{bytecode_suffixes}

%dir %{pylibdir}/concurrent/futures/
%dir %{pylibdir}/concurrent/futures/__pycache__/
%{pylibdir}/concurrent/futures/*.py
%{pylibdir}/concurrent/futures/__pycache__/*%{bytecode_suffixes}

%{pylibdir}/pydoc_data

%{dynload_dir}/_blake2.%{SOABI_optimized}.so
%{dynload_dir}/_md5.%{SOABI_optimized}.so
%{dynload_dir}/_sha1.%{SOABI_optimized}.so
%{dynload_dir}/_sha256.%{SOABI_optimized}.so
%{dynload_dir}/_sha3.%{SOABI_optimized}.so
%{dynload_dir}/_sha512.%{SOABI_optimized}.so
%{dynload_dir}/_asyncio.%{SOABI_optimized}.so
%{dynload_dir}/_bisect.%{SOABI_optimized}.so
%{dynload_dir}/_bz2.%{SOABI_optimized}.so
%{dynload_dir}/_codecs_cn.%{SOABI_optimized}.so
%{dynload_dir}/_codecs_hk.%{SOABI_optimized}.so
%{dynload_dir}/_codecs_iso2022.%{SOABI_optimized}.so
%{dynload_dir}/_codecs_jp.%{SOABI_optimized}.so
%{dynload_dir}/_codecs_kr.%{SOABI_optimized}.so
%{dynload_dir}/_codecs_tw.%{SOABI_optimized}.so
%{dynload_dir}/_contextvars.%{SOABI_optimized}.so
%{dynload_dir}/_crypt.%{SOABI_optimized}.so
%{dynload_dir}/_csv.%{SOABI_optimized}.so
%{dynload_dir}/_ctypes.%{SOABI_optimized}.so
%{dynload_dir}/_curses.%{SOABI_optimized}.so
%{dynload_dir}/_curses_panel.%{SOABI_optimized}.so
%{dynload_dir}/_dbm.%{SOABI_optimized}.so
%{dynload_dir}/_decimal.%{SOABI_optimized}.so
%{dynload_dir}/_elementtree.%{SOABI_optimized}.so
%if %{with gdbm}
%{dynload_dir}/_gdbm.%{SOABI_optimized}.so
%endif
%{dynload_dir}/_hashlib.%{SOABI_optimized}.so
%{dynload_dir}/_heapq.%{SOABI_optimized}.so
%{dynload_dir}/_json.%{SOABI_optimized}.so
%{dynload_dir}/_lsprof.%{SOABI_optimized}.so
%{dynload_dir}/_lzma.%{SOABI_optimized}.so
%{dynload_dir}/_multibytecodec.%{SOABI_optimized}.so
%{dynload_dir}/_multiprocessing.%{SOABI_optimized}.so
%{dynload_dir}/_opcode.%{SOABI_optimized}.so
%{dynload_dir}/_pickle.%{SOABI_optimized}.so
%{dynload_dir}/_posixsubprocess.%{SOABI_optimized}.so
%{dynload_dir}/_queue.%{SOABI_optimized}.so
%{dynload_dir}/_random.%{SOABI_optimized}.so
%{dynload_dir}/_socket.%{SOABI_optimized}.so
%{dynload_dir}/_sqlite3.%{SOABI_optimized}.so
%{dynload_dir}/_ssl.%{SOABI_optimized}.so
%{dynload_dir}/_statistics.%{SOABI_optimized}.so
%{dynload_dir}/_struct.%{SOABI_optimized}.so
%{dynload_dir}/_typing.%{SOABI_optimized}.so
%{dynload_dir}/array.%{SOABI_optimized}.so
%{dynload_dir}/audioop.%{SOABI_optimized}.so
%{dynload_dir}/binascii.%{SOABI_optimized}.so
%{dynload_dir}/cmath.%{SOABI_optimized}.so
%{dynload_dir}/_datetime.%{SOABI_optimized}.so
%{dynload_dir}/fcntl.%{SOABI_optimized}.so
%{dynload_dir}/grp.%{SOABI_optimized}.so
%{dynload_dir}/math.%{SOABI_optimized}.so
%{dynload_dir}/mmap.%{SOABI_optimized}.so
%{dynload_dir}/nis.%{SOABI_optimized}.so
%{dynload_dir}/ossaudiodev.%{SOABI_optimized}.so
%{dynload_dir}/_posixshmem.%{SOABI_optimized}.so
%{dynload_dir}/pyexpat.%{SOABI_optimized}.so
%{dynload_dir}/readline.%{SOABI_optimized}.so
%{dynload_dir}/resource.%{SOABI_optimized}.so
%{dynload_dir}/select.%{SOABI_optimized}.so
%{dynload_dir}/spwd.%{SOABI_optimized}.so
%{dynload_dir}/syslog.%{SOABI_optimized}.so
%{dynload_dir}/termios.%{SOABI_optimized}.so
%{dynload_dir}/unicodedata.%{SOABI_optimized}.so
%{dynload_dir}/_uuid.%{SOABI_optimized}.so
%{dynload_dir}/xxlimited.%{SOABI_optimized}.so
%{dynload_dir}/xxlimited_35.%{SOABI_optimized}.so
%{dynload_dir}/_xxsubinterpreters.%{SOABI_optimized}.so
%{dynload_dir}/zlib.%{SOABI_optimized}.so
%{dynload_dir}/_zoneinfo.%{SOABI_optimized}.so

%dir %{pylibdir}/site-packages/
%dir %{pylibdir}/site-packages/__pycache__/
%{pylibdir}/site-packages/README.txt

%{pylibdir}/*.py
%dir %{pylibdir}/__pycache__/
%{pylibdir}/__pycache__/*%{bytecode_suffixes}

%dir %{pylibdir}/collections/
%dir %{pylibdir}/collections/__pycache__/
%{pylibdir}/collections/*.py
%{pylibdir}/collections/__pycache__/*%{bytecode_suffixes}

%dir %{pylibdir}/ctypes/
%dir %{pylibdir}/ctypes/__pycache__/
%{pylibdir}/ctypes/*.py
%{pylibdir}/ctypes/__pycache__/*%{bytecode_suffixes}
%{pylibdir}/ctypes/macholib

%{pylibdir}/curses

%dir %{pylibdir}/dbm/
%dir %{pylibdir}/dbm/__pycache__/
%{pylibdir}/dbm/*.py
%{pylibdir}/dbm/__pycache__/*%{bytecode_suffixes}

%dir %{pylibdir}/distutils/
%dir %{pylibdir}/distutils/__pycache__/
%{pylibdir}/distutils/*.py
%{pylibdir}/distutils/__pycache__/*%{bytecode_suffixes}
%{pylibdir}/distutils/README
%{pylibdir}/distutils/command

%dir %{pylibdir}/email/
%dir %{pylibdir}/email/__pycache__/
%{pylibdir}/email/*.py
%{pylibdir}/email/__pycache__/*%{bytecode_suffixes}
%{pylibdir}/email/mime
%doc %{pylibdir}/email/architecture.rst

%{pylibdir}/encodings

%{pylibdir}/html
%{pylibdir}/http

%dir %{pylibdir}/importlib/
%dir %{pylibdir}/importlib/__pycache__/
%{pylibdir}/importlib/*.py
%{pylibdir}/importlib/__pycache__/*%{bytecode_suffixes}

%dir %{pylibdir}/importlib/metadata/
%dir %{pylibdir}/importlib/metadata/__pycache__/
%{pylibdir}/importlib/metadata/*.py
%{pylibdir}/importlib/metadata/__pycache__/*%{bytecode_suffixes}

%dir %{pylibdir}/importlib/resources/
%dir %{pylibdir}/importlib/resources/__pycache__/
%{pylibdir}/importlib/resources/*.py
%{pylibdir}/importlib/resources/__pycache__/*%{bytecode_suffixes}

%dir %{pylibdir}/json/
%dir %{pylibdir}/json/__pycache__/
%{pylibdir}/json/*.py
%{pylibdir}/json/__pycache__/*%{bytecode_suffixes}

%{pylibdir}/logging
%{pylibdir}/multiprocessing

%dir %{pylibdir}/re/
%{pylibdir}/re/*.py
%{pylibdir}/re/__pycache__/*%{bytecode_suffixes}

%dir %{pylibdir}/sqlite3/
%dir %{pylibdir}/sqlite3/__pycache__/
%{pylibdir}/sqlite3/*.py
%{pylibdir}/sqlite3/__pycache__/*%{bytecode_suffixes}

%dir %{pylibdir}/tomllib/
%{pylibdir}/tomllib/*.py
%{pylibdir}/tomllib/__pycache__/*%{bytecode_suffixes}
%exclude %{pylibdir}/turtle.py
%exclude %{pylibdir}/__pycache__/turtle*%{bytecode_suffixes}

%{pylibdir}/urllib
%{pylibdir}/xml
%{pylibdir}/zoneinfo

%dir %{pylibdir}/__phello__
%{pylibdir}/__phello__/__init__.py
%{pylibdir}/__phello__/spam.py
%{pylibdir}/__phello__/__pycache__/*%{bytecode_suffixes}

%if "%{_lib}" == "lib64"
%attr(0755,root,root) %dir %{_prefix}/lib/python%{pybasever}
%attr(0755,root,root) %dir %{_prefix}/lib/python%{pybasever}/site-packages
%attr(0755,root,root) %dir %{_prefix}/lib/python%{pybasever}/site-packages/__pycache__/
%endif

%dir %{pylibdir}/config-%{LDVERSION_optimized}-%{platform_triplet}/
%{pylibdir}/config-%{LDVERSION_optimized}-%{platform_triplet}/Makefile
%dir %{_includedir}/python%{LDVERSION_optimized}/
%{_includedir}/python%{LDVERSION_optimized}/%{_pyconfig_h}

%{_libdir}/%{py_INSTSONAME_optimized}
%if %{with main_python}
%{_libdir}/libpython3.so
%endif


%files -n %{pkgname}-devel
%doc Misc/README.valgrind Misc/valgrind-python.supp Misc/gdbinit
%if %{with main_python}
%{_bindir}/2to3
%{_bindir}/python3-config
%{_bindir}/python-config
%{_libdir}/pkgconfig/python3.pc
%{_libdir}/pkgconfig/python.pc
%{_libdir}/pkgconfig/python3-embed.pc
%{_bindir}/pathfix.py
%{_bindir}/pygettext3.py
%{_bindir}/pygettext.py
%{_bindir}/msgfmt3.py
%{_bindir}/msgfmt.py
%endif
%{_bindir}/2to3-%{pybasever}
%{_bindir}/pathfix%{pybasever}.py
%{_bindir}/pygettext%{pybasever}.py
%{_bindir}/msgfmt%{pybasever}.py
%{_bindir}/python%{pybasever}-config
%{_bindir}/python%{LDVERSION_optimized}-config
%{_bindir}/python%{LDVERSION_optimized}-*-config
%{_libdir}/libpython%{LDVERSION_optimized}.so
%{_libdir}/pkgconfig/python-%{LDVERSION_optimized}.pc
%{_libdir}/pkgconfig/python-%{LDVERSION_optimized}-embed.pc
%{_libdir}/pkgconfig/python-%{pybasever}.pc
%{_libdir}/pkgconfig/python-%{pybasever}-embed.pc
%{pylibdir}/config-%{LDVERSION_optimized}-%{platform_triplet}/*
%exclude %{pylibdir}/config-%{LDVERSION_optimized}-%{platform_triplet}/Makefile
%exclude %{_includedir}/python%{LDVERSION_optimized}/%{_pyconfig_h}
%{_includedir}/python%{LDVERSION_optimized}/*.h
%{_includedir}/python%{LDVERSION_optimized}/internal/
%{_includedir}/python%{LDVERSION_optimized}/cpython/

%files -n %{pkgname}-idle
%if %{with main_python}
%{_bindir}/idle*
%{_datadir}/applications/idle3.desktop
%{_datadir}/icons/hicolor/*/apps/idle3.*
%else
%{_bindir}/idle%{pybasever}
%endif
%{pylibdir}/idlelib

%files -n %{pkgname}-tkinter
%{pylibdir}/tkinter
%exclude %{pylibdir}/tkinter/test
%{dynload_dir}/_tkinter.%{SOABI_optimized}.so
%{pylibdir}/turtle.py
%{pylibdir}/__pycache__/turtle*%{bytecode_suffixes}
%dir %{pylibdir}/turtledemo
%{pylibdir}/turtledemo/*.py
%{pylibdir}/turtledemo/*.cfg
%dir %{pylibdir}/turtledemo/__pycache__/
%{pylibdir}/turtledemo/__pycache__/*%{bytecode_suffixes}


%files -n %{pkgname}-test
%{pylibdir}/ctypes/test
%{pylibdir}/distutils/tests
%{pylibdir}/test
%{pylibdir}/lib2to3/tests
%{pylibdir}/tkinter/test
%{pylibdir}/unittest/test
%{dynload_dir}/_ctypes_test.%{SOABI_optimized}.so
%{dynload_dir}/_testbuffer.%{SOABI_optimized}.so
%{dynload_dir}/_testcapi.%{SOABI_optimized}.so
%{dynload_dir}/_testclinic.%{SOABI_optimized}.so
%{dynload_dir}/_testimportmultiple.%{SOABI_optimized}.so
%{dynload_dir}/_testinternalcapi.%{SOABI_optimized}.so
%{dynload_dir}/_testmultiphase.%{SOABI_optimized}.so
%{dynload_dir}/_xxtestfuzz.%{SOABI_optimized}.so

%if %{with debug_build}
%files -n %{pkgname}-debug
%if %{with main_python}
%{_bindir}/python3-debug
%{_bindir}/python-debug
%endif

%{_bindir}/python%{LDVERSION_debug}
%{dynload_dir}/_blake2.%{SOABI_debug}.so
%{dynload_dir}/_md5.%{SOABI_debug}.so
%{dynload_dir}/_sha1.%{SOABI_debug}.so
%{dynload_dir}/_sha256.%{SOABI_debug}.so
%{dynload_dir}/_sha3.%{SOABI_debug}.so
%{dynload_dir}/_sha512.%{SOABI_debug}.so
%{dynload_dir}/_asyncio.%{SOABI_debug}.so
%{dynload_dir}/_bisect.%{SOABI_debug}.so
%{dynload_dir}/_bz2.%{SOABI_debug}.so
%{dynload_dir}/_codecs_cn.%{SOABI_debug}.so
%{dynload_dir}/_codecs_hk.%{SOABI_debug}.so
%{dynload_dir}/_codecs_iso2022.%{SOABI_debug}.so
%{dynload_dir}/_codecs_jp.%{SOABI_debug}.so
%{dynload_dir}/_codecs_kr.%{SOABI_debug}.so
%{dynload_dir}/_codecs_tw.%{SOABI_debug}.so
%{dynload_dir}/_contextvars.%{SOABI_debug}.so
%{dynload_dir}/_crypt.%{SOABI_debug}.so
%{dynload_dir}/_csv.%{SOABI_debug}.so
%{dynload_dir}/_ctypes.%{SOABI_debug}.so
%{dynload_dir}/_curses.%{SOABI_debug}.so
%{dynload_dir}/_curses_panel.%{SOABI_debug}.so
%{dynload_dir}/_dbm.%{SOABI_debug}.so
%{dynload_dir}/_decimal.%{SOABI_debug}.so
%{dynload_dir}/_elementtree.%{SOABI_debug}.so
%if %{with gdbm}
%{dynload_dir}/_gdbm.%{SOABI_debug}.so
%endif
%{dynload_dir}/_hashlib.%{SOABI_debug}.so
%{dynload_dir}/_heapq.%{SOABI_debug}.so
%{dynload_dir}/_json.%{SOABI_debug}.so
%{dynload_dir}/_lsprof.%{SOABI_debug}.so
%{dynload_dir}/_lzma.%{SOABI_debug}.so
%{dynload_dir}/_multibytecodec.%{SOABI_debug}.so
%{dynload_dir}/_multiprocessing.%{SOABI_debug}.so
%{dynload_dir}/_opcode.%{SOABI_debug}.so
%{dynload_dir}/_pickle.%{SOABI_debug}.so
%{dynload_dir}/_posixsubprocess.%{SOABI_debug}.so
%{dynload_dir}/_queue.%{SOABI_debug}.so
%{dynload_dir}/_random.%{SOABI_debug}.so
%{dynload_dir}/_socket.%{SOABI_debug}.so
%{dynload_dir}/_sqlite3.%{SOABI_debug}.so
%{dynload_dir}/_ssl.%{SOABI_debug}.so
%{dynload_dir}/_statistics.%{SOABI_debug}.so
%{dynload_dir}/_struct.%{SOABI_debug}.so
%{dynload_dir}/_typing.%{SOABI_debug}.so
%{dynload_dir}/array.%{SOABI_debug}.so
%{dynload_dir}/audioop.%{SOABI_debug}.so
%{dynload_dir}/binascii.%{SOABI_debug}.so
%{dynload_dir}/cmath.%{SOABI_debug}.so
%{dynload_dir}/_datetime.%{SOABI_debug}.so
%{dynload_dir}/fcntl.%{SOABI_debug}.so
%{dynload_dir}/grp.%{SOABI_debug}.so
%{dynload_dir}/math.%{SOABI_debug}.so
%{dynload_dir}/mmap.%{SOABI_debug}.so
%{dynload_dir}/nis.%{SOABI_debug}.so
%{dynload_dir}/ossaudiodev.%{SOABI_debug}.so
%{dynload_dir}/_posixshmem.%{SOABI_debug}.so
%{dynload_dir}/pyexpat.%{SOABI_debug}.so
%{dynload_dir}/readline.%{SOABI_debug}.so
%{dynload_dir}/resource.%{SOABI_debug}.so
%{dynload_dir}/select.%{SOABI_debug}.so
%{dynload_dir}/spwd.%{SOABI_debug}.so
%{dynload_dir}/syslog.%{SOABI_debug}.so
%{dynload_dir}/termios.%{SOABI_debug}.so
%{dynload_dir}/unicodedata.%{SOABI_debug}.so
%{dynload_dir}/_uuid.%{SOABI_debug}.so
%{dynload_dir}/xxlimited.%{SOABI_debug}.so
%{dynload_dir}/xxlimited_35.%{SOABI_debug}.so
%{dynload_dir}/_xxsubinterpreters.%{SOABI_debug}.so
%{dynload_dir}/_xxtestfuzz.%{SOABI_debug}.so
%{dynload_dir}/zlib.%{SOABI_debug}.so
%{dynload_dir}/_zoneinfo.%{SOABI_debug}.so
%{_libdir}/%{py_INSTSONAME_debug}
%{pylibdir}/config-%{LDVERSION_debug}-%{platform_triplet}
%{_includedir}/python%{LDVERSION_debug}
%{_bindir}/python%{LDVERSION_debug}-config
%{_bindir}/python%{LDVERSION_debug}-*-config
%{_libdir}/libpython%{LDVERSION_debug}.so
%{_libdir}/libpython%{LDVERSION_debug}.so.%{py_SOVERSION}
%{_libdir}/pkgconfig/python-%{LDVERSION_debug}.pc
%{_libdir}/pkgconfig/python-%{LDVERSION_debug}-embed.pc
%{dynload_dir}/_tkinter.%{SOABI_debug}.so
%{dynload_dir}/_ctypes_test.%{SOABI_debug}.so
%{dynload_dir}/_testbuffer.%{SOABI_debug}.so
%{dynload_dir}/_testcapi.%{SOABI_debug}.so
%{dynload_dir}/_testclinic.%{SOABI_debug}.so
%{dynload_dir}/_testimportmultiple.%{SOABI_debug}.so
%{dynload_dir}/_testinternalcapi.%{SOABI_debug}.so
%{dynload_dir}/_testmultiphase.%{SOABI_debug}.so
%{pylibdir}/_sysconfigdata_%{ABIFLAGS_debug}_linux_%{platform_triplet}.py
%{pylibdir}/__pycache__/_sysconfigdata_%{ABIFLAGS_debug}_linux_%{platform_triplet}%{bytecode_suffixes}
%endif

%if %{with main_python}
%files -n python-unversioned-command
%{_bindir}/python
%{_mandir}/*/python.1*
%endif

%changelog
* Mon Mar 10 2025 Shuo Wang <abushwang@tencent.com> - 3.11.6-16
- fix CVE-2025-1795
- email/_header_value_parser: don't encode list separators (GH-100885) (GH-115593)
- Fix UnicodeEncodeError in TokenList.fold() (GH-113730)
- Fix for email.generator.Generator with whitespace between encoded words. (#92281)
- Fix AttributeError in the email module (GH-119099)

* Mon Feb 24 2025 cunshunxia <cunshunxia@tencent.com> - 3.11.6-15
- fix CVE-2025-0938.
- gh-105704: Disallow square brackets (`[` and `]`) in
 domain names for parsed URLs (GH-129418) (#129528)

* Tue Nov 5 2024 Shuo Wang <abushwang@tencent.com> - 3.11.6-14
- fix CVE-2024-6923
- Quote template strings in `venv` activation scripts (GH-124712) (GH-126185)

* Tue Oct 15 2024 cunshunxia <cunshunxia@tencent.com> - 3.11.6-13
- fix CVE-2024-6923.

* Thu Sep 26 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 3.11.6-12
- Rebuilt for clarifying the packages requirement in BaseOS and AppStream

* Wed Sep 18 2024 cunshunxia <cunshunxia@tencent.com> - 3.11.6-11
- fix CVE-2024-8088, CVE-2023-6597 and CVE-2024-7592.

* Fri Sep 6 2024 Shuo Wang <abushwang@tencent.com> - 3.11.6-10
- fix CVE-2024-6232
- Remove backtracking when parsing tarfile headers (GH-121286) (#123639)

* Fri Aug 16 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 3.11.6-9
- Rebuilt for loongarch release

* Wed Jun 19 2024 Shuo Wang <abushwang@tencent.com> - 3.11.6-8
- fix CVE-2024-0397
- Fix "private" (non-global) IP address ranges (GH-113179) (GH-113186)

* Tue Jun 18 2024 Shuo Wang <abushwang@tencent.com> - 3.11.6-7
- fix CVE-2024-0397
- Fix locking in cert_store_stats and get_ca_certs

* Sat May 25 2024 Huang Yang <huangyang@loongson.cn> - 3.11.6-6
- [Type] other
- [DESC] add loongarch64 support

* Mon Apr 15 2024 cunshunxia <cunshunxia@tencent.com> - 3.11.6-5
- fix CVE-2024-0450.

* Wed Apr 10 2024 Shuo Wang <abushwang@tencent.com> - 3.11.6-4
- simplify pybasever macro

* Mon Apr 8 2024 Shuo Wang <abushwang@tencent.com> - 3.11.6-3
- add support for multiple version
- fix etree test fail

* Wed Oct 25 2023 Shuo Wang <abushwang@tencent.com> - 3.11.6-2
- add Recommends to %{_bindir}/python
- default install in user environment and not install in koji

* Mon Oct 23 2023 Zhao Zhen <jeremiazhao@tencent.com> - 3.11.6-1
- upgraded upstream 3.11.6
- fixed CVE-2023-40217 CVE-2023-41105

* Wed Oct 18 2023 Shuo Wang <abushwang@tencent.com> - 3.11.4-4
- remove unused idle3 files

* Wed Sep 20 2023 Shuo Wang <abushwang@tencent.com> - 3.11.4-3
- provides python-unversioned-command

* Thu Aug 24 2023 Zhao Zhen <jeremiazhao@tencent.com> - 3.11.4-2
- small fixes

* Thu Aug 24 2023 Zhao Zhen <jeremiazhao@tencent.com> - 3.11.4-1
- upgraded for upstream 3.11.4
- fixed CVE-2023-24329, CVE-2022-45061, CVE-2022-42919, CVE-2021-28861, CVE-2020-10735, CVE-2015-20107
- removed python-unversioned-command, it is now merged into python3 package, so python3 package contains
- /usr/bin/python by default
